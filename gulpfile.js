var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var watch = require('gulp-watch');
var sass = require('gulp-ruby-sass');
var rename = require('gulp-rename');
var browserSync = require('browser-sync').create();

var assetsPath = 'packages/nomorejack/themes/nomorejack/assets';

gulp.task('watch', function() {
  gulp.watch(assetsPath + '/scss/**/*.scss', ['browser-sync-reload-sass']);
  gulp.watch(assetsPath + '/js/**/*.js', ['browser-sync-reload']);
});

gulp.task('sass', function () {
  return sass(assetsPath + '/scss/style.scss', { sourcemap: true })
    .pipe(gulp.dest(assetsPath + '/css'))
});

gulp.task('minify-css', ['sass'], function() {
  return gulp.src(assetsPath + '/css/style.css')
    .pipe(minifyCss({compatibility: 'ie8'}))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(assetsPath + '/css'));
});

gulp.task('browser-sync-init', function() {
    browserSync.init({
        proxy: 'localhost'
    });
});

gulp.task('browser-sync-reload', function() {
  browserSync.reload();
});

gulp.task('browser-sync-reload-sass', ['sass'], function() {
  browserSync.reload();
});

gulp.task('default', ['browser-sync-init', 'watch'], function() {

});
