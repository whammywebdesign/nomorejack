<?php
  Loader::packageElement('header', 'nomorejack');
  $image = $c->getAttribute('hero_image');
  if ($image):
    $image_src = $image->getRelativePath();
    $image_title = $image->getTitle();
  endif;
?>

  <section class="section">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--8 grid__shift--2 grid__col--sm--12 color--white">
          <?php
            $a = new Area('Content');
            $a->display($c);
          ?>
          <br /><br />
          <a class="center--left btn btn--light-gray btn--medium text--center" href="<?php echo View::url('/') ?>">Back to Home</a>
        </div>
      </div>
    </div>
  </section>

<?php Loader::packageElement('footer', 'nomorejack'); ?>