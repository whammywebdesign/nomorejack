<?php

namespace Concrete\Package\NoMoreJack;

use Concrete\Core\Package\Package;
use Concrete\Core\Page\Theme\Theme;
use Core;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package {
  protected $pkgHandle = 'nomorejack';
  protected $appVersionRequired = '5.8.0.3';
  protected $pkgVersion = '1.0.0';

  public function getPackageDescription() {
    return t("Adds No More Jack theme.");
  }

  public function getPackageName(){
    return t("No More Jack");
  }
  public function install() {
    $pkg = parent::install();
    Theme::add('nomorejack', $pkg);
  }
}
