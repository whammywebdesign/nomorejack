<?php
  Loader::packageElement('header', 'nomorejack');
?>

  <section class="hero hero--large">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--8 grid__col--sm--12 hero__inner color--white scroll__reveal--up">
          <?php
            $a = new Area('Hero');
            $a->display($c);
          ?>
          <hr class="underline underline--white no-margin">
        </div>
      </div>
    </div>
  </section>

  <section class="section section__overlay overflow-hidden">
    <div class="grid__container">
      <div class="grid__row">

        <div class="grid__col--6 grid__col--sm--12 color--white">
          <h3>Listen to our music</h3>
          <iframe class="spotify match-height" src="https://embed.spotify.com/?uri=spotify%3Aartist%3A7pY0BBSJCeHVtfmGl4XW65" width="300" height="380" frameborder="0" allowtransparency="true"></iframe>
        </div>

        <div id="events" class="grid__col--6 grid__col--sm--12 color--black">
          <?php
            $a = new Area('Events');
            $a->display($c);
          ?>
        </div>

      </div>
    </div>
  </section>

  <section class="section">
    <div class="grid__container">
      <div class="grid__row">

        <div id="media" class="grid__col--12 color--white">
          <h3>Latest music videos</h3>
          <div class="grid__row">
            <figure class="grid__col--6 grid__col--xs--12 section__content video">
              <div data-type="youtube" data-video-id="https://youtu.be/0VgD2hdJXrY"></div>
              <figcaption>
                <h4>Over Now</h4>
              </figcaption>
            </figure>
            <figure class="grid__col--6 grid__col--xs--12 section__content video">
              <div data-type="youtube" data-video-id="https://youtu.be/1w7LfXNGYP8"></div>
              <figcaption>
                <h4>Mr. A</h4>
              </figcaption>
            </figure>
          </div>
        </div>

      </div>
    </div>
  </section>

  <section class="section">
    <div class="grid__row">

      <div class="grid__col--12 color--white">
        <h3 class="text--center">Photos</h3>
        <div class="grid__row">
          <div class="grid__col--12 section__content">
            <?php
              $a = new Area('Gallery');
              $a->display($c);
            ?>
          </div>
        </div>
      </div>

    </div>
  </section>

  <section class="section background--white no-margin no-padding box-shadow--inside">
    <div class="grid__container">
      <div class="grid__row">

        <div id="about" class="grid__col--12 section__inner color--black">
          <h3 class="color--red text--center">About us</h3>
          <div class="grid__row">
            <div class="grid__col--8 grid__shift--2 grid__col--sm--12 section__content">
              <?php
                $a = new Area('About Text');
                $a->display($c);
              ?>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

<?php Loader::packageElement('footer', 'nomorejack'); ?>