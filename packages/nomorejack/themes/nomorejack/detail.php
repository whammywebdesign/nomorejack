<?php
  Loader::packageElement('header', 'nomorejack');
  // $image = $c->getAttribute('page_image');
  // if ($image):
  //   $image_src = $image->getRelativePath();
  //   $image_title = $image->getTitle();
  // endif;
?>

  <section class="hero hero--medium">
    <div class="grid__container">
      <div class="grid__row">
        <div class="grid__col--8 grid__col--sm--12 hero__inner color--white scroll__reveal--up">
          <?php
            $a = new Area('Hero');
            $a->display($c);
          ?>
          <hr class="underline underline--white no-margin">
        </div>
      </div>
    </div>
  </section>

  <section class="section section__overlay overflow-hidden">
    <div class="grid__container">
      <div class="grid__row">

        <div class="grid__col--6 grid__col--sm--12 color--white">
          <?php
            $a = new Area('Content Left');
            $a->display($c);
          ?>
        </div>

        <div class="grid__col--6 grid__col--sm--12 color--white">
          <?php
            $a = new Area('Content Right');
            $a->display($c);
          ?>
        </div>

      </div>
    </div>
  </section>

<?php Loader::packageElement('footer', 'nomorejack'); ?>