$(document).ready(function() {

  objectFitImages();

  $('.header').next().css({'margin-top':$('.header').outerHeight()});

  $('.navicon').stop().on('click', function(e) {
    e.preventDefault();
    $(this).toggleClass('navicon--open');
    if ($(this).hasClass('navicon--open')) {

      $('html, body').css({'overflow':'hidden'});

      $('.page').animate({
        left: '-250px'
      }, 500);

      setTimeout(function(){
        $('.page .nav').show().addClass('animated slideInRight').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
          function(e) {
            $(this).removeClass('animated slideInRight');
          }
        );
      }, 100);

    } else {
      $('html, body').css({'overflow':'auto'});

      $('.page .nav').addClass('animated slideOutRight').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',
        function(e) {
          $(this).removeClass('animated slideOutRight').hide();
        }
      );

      setTimeout(function(){
        $('.page').animate({
          left: '0'
        }, 500);
      }, 250);

    }
  });

  /**
   * Footer always sticks at the bottom of the page
   */

  var footerHeight;

  function stickyFooter() {
    footerHeight = $('.footer').outerHeight();
    $('.page__wrap').css({'min-height':$(window).height() - footerHeight});
  };

  $(window).bind('resize', stickyFooter);
  stickyFooter();

  /**
   * Match Height
   */

  $('.match-height').matchHeight();

  /**
   * Video
   */

  var video = $('.video');

  if (video.length) {
    var player = plyr.setup('.video');
  }

  /**
   * Image Gallery
   */

  $("#gallery").unitegallery({
    gallery_theme: "tiles",
    theme_gallery_padding: 0,
    tiles_justified_space_between:0,
    tiles_type: "justified",
    tiles_space_between_cols: 0,
    tiles_space_between_cols_mobile: 0,
    tile_enable_image_effect: true,
    tile_image_effect_type: "blur",
    tile_image_effect_reverse: true,
  });

  /**
   * Smoothscroll
   */

  $(window).scroll(function() {
    if ($(document).scrollTop() >= 75) {
      $('.header__background').show().addClass('animated slideInDown').one('webkitAnimationEnd oanimationend msAnimationEnd animationend');
    } else {
      $('.header__background').slideUp(200);
    }
  });

  if ($(window).scrollTop() > 75) {
    $('.header__background').show().addClass('animated slideInDown').one('webkitAnimationEnd oanimationend msAnimationEnd animationend');
  } else {
    $('.header__background').hide();
  }

  $(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top - $('.header').outerHeight() - 40
          }, 1000);
          return false;
        }
      }
    });
  });

  /**
   * Scrollreveal
   */

  window.sr = ScrollReveal();

  sr.reveal('.scroll__reveal');

  sr.reveal('.scroll__reveal--up', {
    origin: 'bottom',
    distance: '100%'
  });

  sr.reveal('.scroll__reveal--left', {
    origin: 'left',
    distance: '100%'
  });

  sr.reveal('.scroll__reveal--right', {
    origin: 'right',
    distance: '100%'
  });

});