<?php
  defined('C5_EXECUTE') or die("Access Denied.");
  $th = Loader::helper('text');
  $dh = Core::make('helper/date');
  $c = Page::getCurrentPage();
?>

<?php if (isset($pageListTitle) && $pageListTitle): ?>
  <h3 class="color--white"><?php echo h($pageListTitle); ?></h3>
<?php endif; ?>

<div class="grid__col--12 event section__content background--white">
<?php
  foreach ($pages as $page):

    $title = $th->entities($page->getCollectionName());
    $url = $nh->getLinkToCollection($page);
    $description = $page->getCollectionDescription();
    $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
    $description = $th->entities($description);
    $date = $dh->formatDateTime($page->getCollectionDatePublic(), true);

    $eventLocation = $page->getAttribute('event_location');
    $eventVenue = $page->getAttribute('event_venue');
    $eventLink = $page->getAttribute('event_link');
?>

  <figure class="event__figure">
    <time class="text--center" datetime="<?php echo $page->getCollectionDatePublic(); ?>"><?php echo $dh->formatCustom('M', $page->getCollectionDatePublic()); ?> <span><?php echo $dh->formatCustom('d', $page->getCollectionDatePublic()); ?></span></time>
    <div class="event__inner">
      <span><i class="fa fa-map-marker"></i></span>
      <figcaption>
        <strong class="color--black"><?php echo $eventLocation; ?></strong>
        <p class="color--light-gray"><?php echo $eventVenue; ?></p>
      </figcaption>
    </div>

    <?php if ($eventLink) { ?>
    <a class="btn" target="_blank" href="<?php echo $eventLink; ?>"><i class="fa fa-chevron-right"></i></a>
    <?php } ?>
  </figure>

<?php endforeach; ?>

  <?php if (count($pages) == 0): ?>
  <figure class="event__figure">
    <h4 class="text--center">No upcoming events</h4>
  </figure>
  <?php endif;?>

  <?php if ($c->getCollectionID() == 1) { ?>
  <figure class="event__figure">
    <a class="event__btn btn width--stretch float--left text--center" href="<?php echo View::url('/events/past-events'); ?>">See our past events</a>
  </figure>
  <?php } ?>

<?php if ($showPagination): ?>
  <?php echo $pagination;?>
<?php endif; ?>
  <?php if ($c->getCollectionID() == 1) { ?>
  <a class="event__more btn btn--light-gray btn--full btn--large text--center" href="http://www.gigstarter.nl/artiesten/no-more-jack" target="_blank">Book us now!</a>
  <?php } ?>
</div>
<?php if ($c->isEditMode() && $controller->isBlockEmpty()): ?>
  <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.')?></div>
<?php endif; ?>