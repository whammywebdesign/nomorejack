<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div id="ccm-block-social-links<?php echo $bID?>" class="ccm-block-social-links">
  <ul class="center--left float--left list-inline">
  <?php foreach($links as $link) {
      $service = $link->getServiceObject();
      ?>
      <li class="hvr-grow"><a class="center--middle float--left color--red" target="_blank" href="<?php echo h($link->getURL()); ?>"><?php echo $service->getServiceIconHTML(); ?></a></li>
  <?php } ?>
  </ul>
</div>