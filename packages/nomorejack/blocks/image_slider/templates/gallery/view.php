<?php
  defined('C5_EXECUTE') or die("Access Denied.");
  $c = Page::getCurrentPage();
  $ih = Core::make('helper/image');
  if ($c->isEditMode()) {
?>
  <div class="ccm-edit-mode-disabled-item" style="<?php echo isset($width) ? "width: $width;" : '' ?><?php echo isset($height) ? "height: $height;" : '' ?>">
    <i style="font-size:40px; margin-bottom:20px; display:block;" class="fa fa-picture-o" aria-hidden="true"></i>
    <div style="padding: 40px 0px 40px 0px"><?php echo t('Image Slider disabled in edit mode.')?>
  		<div style="margin-top: 15px; font-size:9px;">
  			<i class="fa fa-circle" aria-hidden="true"></i>
  			<?php if (count($rows) > 0) { ?>
				<?php foreach (array_slice($rows, 1) as $row) { ?>
				<i class="fa fa-circle-thin" aria-hidden="true"></i>
				<?php }
				}
  			?>
  		</div>
    </div>
  </div>
<?php
  } else {
    if (count($rows) > 0) {
?>
  <div id="gallery" class="gallery">
<?php
      foreach ($rows as $row) {
        $f = File::getByID($row['fID']);
        if (is_object($f)) {
?>
    <img class="gallery__item" src="<?php echo $ih->getThumbnail($f, 1920, 1080)->src; ?>" alt="<?php echo $row['title']; ?>" />
<?php
        }
      }
?>
  </div>
<?php
    } else {
?>
  <div class="ccm-image-slider-placeholder">
    <p><?php echo t('No Slides Entered.'); ?></p>
  </div>
  <?php } ?>
<?php } ?>
