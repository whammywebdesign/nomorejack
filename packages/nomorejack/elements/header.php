<?php
	$user = new User();
	$c = Page::getCurrentPage();
?>
<!doctype html>
<html lang="nl">
<head>
	<?php Loader::element('header_required') ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="<?php echo $view->getThemePath() ?>/vendor/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
  <link href="<?php echo $view->getThemePath() ?>/vendor/plyr/dist/plyr.css" type="text/css" rel="stylesheet" />
  <link href="<?php echo $view->getThemePath() ?>/vendor/unitegallery/dist/css/unite-gallery.css" type="text/css" rel="stylesheet" />
  <link href="<?php echo $view->getThemePath() ?>/vendor/hover.css/css/hover-min.css" type="text/css" rel="stylesheet" />
  <link href="<?php echo $view->getThemePath() ?>/vendor/animate.css/animate.min.css" type="text/css" rel="stylesheet" />
  <link href="<?php echo $view->getThemePath() ?>/assets/css/style.css" type="text/css" rel="stylesheet" />
</head>
<body>

<div class="page">
  <div class="page__wrap box-shadow--bottom">

    <header class="header">
      <div class="header__background background--black-transparent"></div>
      <div class="grid__container">
        <div class="grid__row">
          <div class="grid__col--12">
            <a class="logo" href="<?php echo View::url('/'); ?>"></a>
            <nav class="nav">
              <?php if ($c->getCollectionID() != 152): ?>
              <ul class="nav__list">
                <li class="nav__item"><a href="#events">Events</a></li>
                <li class="nav__item"><a href="#media">Media</a></li>
                <li class="nav__item"><a href="#about">About us</a></li>
              </ul>
              <?php endif; ?>
              <?php
                $a = new GlobalArea('Social Media Links');
                $a->display($c);
              ?>
            </nav>
            <a class="navicon" href="#">
              <div class="navicon__bars"></div>
            </a>
          </div>
        </div>
      </div>
    </header>