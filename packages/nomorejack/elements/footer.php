    </div><!-- END PAGE WRAP -->

    <footer class="footer background--black color--white">
      <div class="grid__container">
        <div class="grid__row gutter">

          <div class="grid__col--12 color--white">
            <h3 class="text--center">Follow us on social media</h3>
            <div class="grid__row">
              <div class="grid__col--8 grid__shift--2 grid__col--sm--12 section__content">
                <?php
                  $a = new GlobalArea('Social Media Links');
                  $a->display($c);
                ?>
              </div>
            </div>
            <div class="grid__row">
              <div class="grid__col--8 grid__shift--2 grid__col--sm--12">
                <br /><br />
                <a class="center--left btn btn--red btn--large text--center" href="http://www.gigstarter.nl/artiesten/no-more-jack" target="_blank">Book us now!</a>
              </div>
            </div>
          </div>

        </div>
        <div class="grid__row gutter">
          <hr class="underline underline--gray">
          <div class="grid__col--12 footer__bottom text--center">
            <small>&copy; Copyright <?php echo date('Y'); ?> by <strong>No More Jack</strong>. All rights reserved.</small>
          </div>
        </div>
      </div>
    </footer>

  </div><!-- END PAGE -->

  <?php Loader::element('footer_required') ?>

  <?php $u = new User(); if (!$u->isLoggedIn()): ?>
  <script src="<?php echo $view->getThemePath() ?>/vendor/jquery/dist/jquery.min.js" type="text/javascript"></script>
  <?php endif; ?>
  <script src="<?php echo $view->getThemePath() ?>/vendor/jquery-match-height/dist/jquery.matchHeight-min.js" type="text/javascript"></script>
 	<script src="<?php echo $view->getThemePath() ?>/vendor/plyr/dist/plyr.js" type="text/javascript"></script>
  <script src="<?php echo $view->getThemePath() ?>/vendor/unitegallery/dist/js/unitegallery.min.js" type="text/javascript"></script>
  <script src="<?php echo $view->getThemePath() ?>/vendor/unitegallery/dist/themes/tiles/ug-theme-tiles.js" type="text/javascript"></script>
  <script src="<?php echo $view->getThemePath() ?>/vendor/scrollreveal/dist/scrollreveal.min.js" type="text/javascript"></script>
  <script src="<?php echo $view->getThemePath() ?>/vendor/object-fit-images/dist/ofi.min.js" type="text/javascript"></script>
  <script src="<?php echo $view->getThemePath() ?>/assets/js/app.js" type="text/javascript"></script>
</body>
</html>
